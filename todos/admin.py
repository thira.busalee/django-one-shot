from django.contrib import admin
from .models import TodoList
from .models import TodoItem
# Register your models here.

@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )

@admin.register(TodoItem)
class  TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
    )
